const redux = require('redux')
const createStore = redux.createStore
const buy_Book = "buy_Book"
const buy_Pen = "buy_Pen"


const initialState = {
    numberOfBooks: 10,
    numberOfPens: 9,
}


function buyBook(){
    return {
        type: buy_Book,
        payload: "My First Redux Code"
    }
}

function buyPen(){
    return {
        type: buy_Pen,
        payload: "My First Redux Code"
    }
}

const Reducer = (state = initialState, action) => {
    switch(action.type) {
        case "buy_Book":
            return {
                ...state,
                numberOfBooks: state.numberOfBooks - 1
            }

        case "buy_Pen":
            return {
                ...state,
                numberOfPens: state.numberOfPens - 1
            }

        default: return state;
    }
}

const store = createStore(Reducer);
console.log('Initial state', store.getState())
const unsubscribe = store.subscribe(()=>{
    console.log('Updated State', store.getState())
})
store.dispatch(buyBook())
store.dispatch(buyBook())
store.dispatch(buyBook())
store.dispatch(buyPen())
store.dispatch(buyPen())
store.dispatch(buyPen())
unsubscribe()